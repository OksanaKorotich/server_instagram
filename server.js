
const express = require('express');
const cors = require('cors');
const { MongoClient, ServerApiVersion } = require('mongodb');

const uri = "mongodb+srv://okorotich:zGJa2Wt49Bal8aTm@clusterinsta.dcgjinm.mongodb.net/?retryWrites=true&w=majority";
const client = new MongoClient(uri, {
  serverApi: {
    version: ServerApiVersion.v1,
    strict: true,
    deprecationErrors: true,
  }
});

const app = express();
app.use(cors());
const port = 5000;

async function run() {
  try {
    await client.connect();
    console.log("Connected to MongoDB!");

    const database = client.db("InstaUsers");
    const collections = await database.listCollections().toArray();

    console.log("Collections in the database:");
    collections.forEach((collection) => {
      console.log(collection.name);
    });
  } catch (error) {
    console.error("Error:", error);
  }
}

run().catch(console.dir);


app.get('/users', async (req, res) => {
  try {
    const database = client.db("InstaUsers");
    const usersCollection = database.collection("users");
    const users = await usersCollection.find().toArray();
    res.json(users);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.get('/posts', async (req, res) => {
  try {
    const database = client.db("InstaUsers");
    const usersCollection = database.collection("posts");
    const posts = await usersCollection.find().toArray();
    res.json(posts);
  } catch (error) {
    console.error("Error:", error);
    res.status(500).json({ error: "Internal Server Error" });
  }
});

app.listen(port, () => {
  console.log(`Server is running on port ${port}`);
});







